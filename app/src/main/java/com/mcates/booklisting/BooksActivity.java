package com.mcates.booklisting;

/*
 * Created by Matt Cates
 * Udacity Android Basics Nanodegree
 * 2-20-2019
 */

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class BooksActivity extends AppCompatActivity {

    private static final String LOG_TAG = BooksActivity.class.getName();
    // URL for google books search.
    private static final String BOOK_REQUEST_URL = "https://www.googleapis.com/books/v1/volumes?q=";
    // My Google API key that attaches to end of URL.
    private static final String API_KEY = "&key=AIzaSyCfRZbEzRYGZRWCySTHQYBDwOMcDgES5gs";
    // Adapter for the list of books.
    private BooksAdapter mAdapter;
    // Layout assets.
    private String searchUrl;
    // Search variables.
    private String searchTerm;
    private EditText userInput;
    // ListView for books.
    private ListView booksListView;
    // Progress bar view.
    private ProgressBar loadingIndicator;
    // Search Google Books message.
    private TextView mainTextView;
    // Tool to check for internet connectivity.
    ConnectivityManager conMan;
    NetworkInfo netStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Set main layout view.
        setContentView(R.layout.books_activity);

        // Hook up progress bar view.
        loadingIndicator = (ProgressBar) findViewById(R.id.loading_indicator);

        // Define the empty TextView that will show when there is no "Internet Connection".
        mainTextView = (TextView) findViewById(R.id.empty_view);
        mainTextView.setText(R.string.search_google_books);
        mainTextView.setVisibility(View.VISIBLE);

        // Tool to check if there is an active internet connection.
        conMan = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        netStatus = conMan.getActiveNetworkInfo();

        // Hide or show no internet text.
        if (netStatus != null && netStatus.isConnected()) {
            // Show "Search Google Books" text view.
            mainTextView.setVisibility(View.VISIBLE);

            // Display no internet connection toast at startup if applicable.
        } else {
            // Show no internet notification if not connected.
            Toast.makeText(getApplicationContext(), "No internet connection!", Toast.LENGTH_SHORT).show();
        }

        // Hide the progress bar view by default.
        loadingIndicator.setVisibility(View.INVISIBLE);

        // Reference to the the {@link ListView in the layout.
        booksListView = (ListView) findViewById(R.id.list);

        // Search button and search input field.
        Button searchBtn = (Button) findViewById(R.id.search_btn);
        userInput = (EditText) findViewById(R.id.edit_text);

        // Create a new adapter that takes an empty list of books as input.
        mAdapter = new BooksAdapter(this, new ArrayList<Books>());

        // OnClick for search button.
        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // InputMethodManager will hide the virtual keyboard.
                InputMethodManager im = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                im.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

                // Search button closes virtual keyboard.
                if (getCurrentFocus() != null) {
                    InputMethodManager inputManager = (InputMethodManager)
                            getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                }

                // Check if there is an active internet connection.
                conMan = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
                netStatus = conMan.getActiveNetworkInfo();

                // Get user input and store it in string.
                searchTerm = userInput.getText().toString();

                // Verify that text has been entered in search field when searching.
                if (searchTerm.trim().length() <= 0 || searchTerm.length() <= 0) {
                    // Display a toast notification if no text has been searched.
                    Toast.makeText(getApplicationContext(), "No Search Entered!", Toast.LENGTH_SHORT).show();

                } else {
                    // Combine the book request url with the user's search keyword and
                    // append the api string to the end.
                    searchUrl = BOOK_REQUEST_URL + searchTerm + API_KEY;
                    booksListView.setAdapter(mAdapter);

                    // Do the following if there is internet connection when searching.
                    if (netStatus != null && netStatus.isConnected()) {
                        mainTextView.setVisibility(View.INVISIBLE);

                        // Show progress bar when searching.
                        loadingIndicator.setVisibility(View.VISIBLE);

                        // Clear list.
                        mAdapter.clear();
                        BooksAsyncTask task = new BooksAsyncTask();

                        // Search Google Books with the user's search query.
                        task.execute(searchUrl);

                    } else {
                        // Clear the list and show "No internet" message if there is no internet.
                        mAdapter.clear();
                        mainTextView.setVisibility(View.VISIBLE);
                        Toast.makeText(getApplicationContext(), "No internet connection!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        // Set an item click listener on the ListView, which sends an intent to a web browser
        // to open a website with more information about the book.
        booksListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {

                // Find the book that was clicked on.
                Books currentBook = mAdapter.getItem(position);

                // Convert the String URL into a URI object (to pass into intent constructor).
                Uri bookUri = Uri.parse(currentBook.getUrl());

                // Create a new intent to view the book URI.
                Intent bookPageIntent = new Intent(Intent.ACTION_VIEW, bookUri);

                // Send the intent to launch a new activity.
                startActivity(bookPageIntent);
            }
        });
    }

    // {@link AsyncTask} to perform the network request on a background thread.
    private class BooksAsyncTask extends AsyncTask<String, Void, List<Books>> {

        // This method runs on a background thread.
        @Override
        protected List<Books> doInBackground(String... urls) {
            // Don't perform the request if there are no URLs, or the first URL is null.
            if (urls.length < 1 || urls[0] == null) {
                return null;
            }
            List<Books> result = QueryUtils.fetchBooksData(urls[0]);
            return result;
        }

        // This method runs on main UI after the background word is completed.
        @Override
        protected void onPostExecute(List<Books> data) {

            // Clear the adapter of previous book data.
            mAdapter.clear();
            // Hide progress bar when search is complete.
            loadingIndicator.setVisibility(View.INVISIBLE);

            // If there is a valid list of {@link Books}, then add them to the adapter's
            // data set. This will trigger the ListView to update.
            if (data != null && !data.isEmpty()) {

                // Hide "Search Google Books' view if results are found.
                mainTextView.setVisibility(View.INVISIBLE);

                // Hide the progress bar.
                loadingIndicator.setVisibility(View.INVISIBLE);

                // Add all results to the adapter data set.
                mAdapter.addAll(data);
                Log.i(LOG_TAG, "TEST: Calling onPostExecute");
            }

            // Display a toast notification that says "No matches found!".
            else {
                mainTextView.setVisibility(View.VISIBLE);
                Toast.makeText(getApplicationContext(), "No matches found!", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
