package com.mcates.booklisting;

public class Books {

    // Global variables.
    private String mAuthor;
    private String mTitle;
    private String mDescription;
    private String mUrl;

    /**
     * Constructor method.
     *
     * @param author
     * @param title
     * @param description
     * @param url
     */
    public Books(String author, String title, String description, String url) {
        mAuthor = author;
        mTitle = title;
        mDescription = description;
        mUrl = url;
    }

    // Getter methods.
    public String getAuthor() {
        return mAuthor;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getDescription() {
        return mDescription;
    }

    public String getUrl() {
        return mUrl;
    }

}
